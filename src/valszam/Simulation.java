package valszam;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Simulation {
	public static void run() {
		double radius = 1;
		Coordinate coordinate;
		int n = 1024;
		int[] tomb = new int[n];
		for (int i = 0; i < 1024 * n; ++i) {
			coordinate = new Coordinate();
			coordinate.generateRandomInASphere(radius);
			coordinate.projectToSphereSurface(radius);
			double x = coordinate.getX();
			x += radius;
			x *= (n) / (2 * radius);
			++tomb[(int) x];
		}
		
		FileWriter fw = null;
		try {
			fw = new FileWriter("felosztott.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter pw = new PrintWriter(fw);
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < tomb[i]; j += 16) {
				pw.print("#");
			}
			pw.println();
		}
	}
	
}
