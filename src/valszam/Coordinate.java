package valszam;

import java.security.SecureRandom;

public class Coordinate {

	private double x, y, z;

	public Coordinate(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Coordinate() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}

	@Override
	public String toString() {
		return "x: " + this.x + "\ny: " + this.y + "\nz: " + this.z;

	}

	public double getX() {
		return this.x;
	}

	public void generateRandomInASphere(double radius) {
		SecureRandom random = new SecureRandom();
		this.x = (random.nextDouble() - 0.5) * 2 * radius;
		this.y = (random.nextDouble() - 0.5) * 2 * radius;
		this.z = (random.nextDouble() - 0.5) * 2 * radius;
		if (this.x * this.x + this.y * this.y + this.z * this.z > radius * radius) {
			this.generateRandomInASphere(radius);
		}
	}

	public void projectToSphereSurface(double radius) {
		double distance = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
		double multiplier = radius / distance;
		this.x *= multiplier;
		this.y *= multiplier;
		this.z *= multiplier;
	}

}
